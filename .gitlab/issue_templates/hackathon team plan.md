## Event summary

(Enter dates for the Hackathon plus any relevant notes for the event such as new themes, prizes, etc.)

Link to main planning issue: 

## Prior to the Hackathon

- Schedule time in your calendar during the hackathon!
- Offer to host an office hours call (team/individual intro, ask-me-anything, and possibly some live coding)
- Create a list of issues with clear implementation plans (preferably suitable for newcomers).
  It would be great to have around 10 well-defined issues for contributors to choose from.

## During the Hackathon

- Ensure you are available to review and assist with contributions
- Stop by the [GitLab Discord server](https://discord.gg/gitlab) and introduce yourself
- Participate (submit that merge request you’ve been postponing or try contributing in an area you are not as familiar with)

## After the Hackathon

- Provide timely reviews and support to ensure merge requests are merged by the due date
- Provide feedback on what worked well and what could be improved
